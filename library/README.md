# clone-fields

[![pipeline status](https://gitlab.com/mexus/fields-converter/badges/master/pipeline.svg)](https://gitlab.com/mexus/fields-converter/commits/master)
[![crates.io](https://img.shields.io/crates/v/clone-fields.svg)](https://crates.io/crates/clone-fields)
[![docs.rs](https://docs.rs/clone-fields/badge.svg)](https://docs.rs/clone-fields)

[[Release docs]](https://docs.rs/clone-fields/)

[[Master docs]](https://mexus.gitlab.io/fields-converter/clone_fields/)

Fields-wise types cloning. Nothing more, nothing less.

```rust
use clone_fields::{CloneFields, CloneInto, CloneFrom};

// PartialEq and Debug traits are only required for `assert` macros in the example.
#[derive(PartialEq, Debug, CloneFields)]
#[destinations("External")]
struct Original<'a, T> {
    field1: &'a i64,
    field2: T,
    nested: OriginalNested,
}

#[derive(PartialEq, Debug, CloneFields)]
#[destinations("ExternalNested", "ExternalNested2")]
struct OriginalNested {
    value: i32,
}

// S2 might be a *foreign* type, i.e. declared in a different crate.
struct External<'a, T> {
    field1: &'a i64,
    field2: T,
    nested: ExternalNested,
}

struct ExternalNested {
    value: i32,
}

// This struct only exists for the sake of using `destinations` attribute with more than one
// type :)
struct ExternalNested2 {
    value: i32,
}

fn main() {
    let obj: Original<_> = Original {
        field1: &0,
        field2: String::from("lol"),
        nested: OriginalNested { value: 15 }
    };
    let cloned: External<_> = obj.clone_into();
    assert_eq!(obj.field1, cloned.field1);
    assert_eq!(obj.field2, cloned.field2);
    assert_eq!(obj.nested.value, cloned.nested.value);
    let cloned2 = Original::clone_from(&cloned);
    assert_eq!(cloned.field1, cloned2.field1);
    assert_eq!(cloned.field2, cloned2.field2);
    assert_eq!(obj, cloned2);
}
```

License: MIT/Apache-2.0
