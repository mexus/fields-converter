# fields-converter-derive

[![pipeline status](https://gitlab.com/mexus/fields-converter/badges/master/pipeline.svg)](https://gitlab.com/mexus/fields-converter/commits/master)
[![crates.io](https://img.shields.io/crates/v/fields-converter-derive.svg)](https://crates.io/crates/fields-converter-derive)
[![docs.rs](https://docs.rs/fields-converter-derive/badge.svg)](https://docs.rs/fields-converter-derive)

[[Release docs]](https://docs.rs/fields-converter-derive/)

[[Master docs]](https://mexus.gitlab.io/fields-converter/fields_converter_derive/)

Collection of procedural macros to allow you "copy", "move" and "duplicate" your structs
fields-wise.

Here's an ultimate example to give you a feel for what you can do with this crate:

```rust
extern crate fields_converter_derive;
extern crate clone_fields;
use clone_fields::{CloneInto, CloneFrom};
use fields_converter_derive::{Duplicate, MoveFields, CloneFields, EqFields, OrdFields};

#[derive(Duplicate, MoveFields, CloneFields, EqFields, OrdFields, Debug)]
#[destinations("Copied")]
#[add_derives(Clone, Debug, PartialEq)]
struct Origin<'a, T> {
  field1: String,
  field2: T,
  field3: &'a str,
}

fn main() {
  let source = Origin {
    field1: "lol".into(),
    field2: 9907,
    field3: "testing",
  };
  // Let's create a `Copied` type from the `Source` (here `CloneFields` shines).
  let copied: Copied<_> = source.clone_into();
  // Now let's clone it using the `add_derives(Clone)`
  let cloned = copied.clone();
  // `assert_eq` requires `Debug` and `PartialEq` traits, which are implemented thanks to
  // `add_derives(Debug, PartialEq)`.
  assert_eq!(copied, cloned);
  // .. and compare it to the source object (thanks `EqFields`!).
  assert_eq!(source, cloned);
  // Ok, let change a field and see that `<` also works (`OrdFields`).
  let greater = Copied {
    field2: source.field2 + 1,
    ..cloned
  };
  assert!(source < greater);
  // ... and vice versa:
  assert!(greater > source);
  // And, finally, let's move `source` into a `Copied` object, conversion sponsored by
  // `MoveFieds`.
  let moved: Copied<_> = source.into();
}
```

License: MIT/Apache-2.0
