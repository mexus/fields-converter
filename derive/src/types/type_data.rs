use super::{enum_data::EnumData, struct_data::StructData};
use proc_macro2::Span;
use syn::{
    self,
    parse::{Error, Result},
    Data,
};

pub struct TypeInfo {
    pub ty: DataType,
    pub name: syn::Ident,
    pub generics: syn::Generics,
}

pub enum DataType {
    Struct(StructData),
    Enum(EnumData),
}

impl TypeInfo {
    pub fn new(data: Data, ident: syn::Ident, generics: syn::Generics) -> Result<Self> {
        let span = ident.span();
        let ty = match data {
            Data::Struct(s) => DataType::Struct(s.into()),
            Data::Enum(e) => DataType::Enum(e.into()),
            Data::Union(_) => {
                return Err(Error::new(span, r#"Only structs and enums are supported"#));
            }
        };
        Ok(TypeInfo {
            ty,
            name: ident,
            generics,
        })
    }

    pub fn add_bound(&mut self, bound: &str) {
        let bound = syn::TraitBound {
            paren_token: None,
            modifier: syn::TraitBoundModifier::None,
            lifetimes: None,
            path: syn::Ident::new(bound, Span::call_site()).into(),
        };
        self.generics.type_params_mut().for_each(|param| {
            param.bounds.push(bound.clone().into());
        });
    }
}
