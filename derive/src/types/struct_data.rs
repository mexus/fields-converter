use super::field::Fields;
use syn;

pub struct StructData {
    pub fields: Fields,
}

impl From<syn::DataStruct> for StructData {
    fn from(syn::DataStruct { fields, .. }: syn::DataStruct) -> Self {
        StructData {
            fields: Fields::from(fields),
        }
    }
}
