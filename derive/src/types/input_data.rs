use super::type_data::TypeInfo;
use syn::{
    self,
    parse::{Error, Parse, ParseStream, Result},
    spanned::Spanned,
};

pub struct InputData {
    pub type_info: TypeInfo,
    pub destination_types: Vec<syn::Path>,
    pub derives: Vec<syn::Ident>,
}

impl Parse for InputData {
    fn parse(input: ParseStream) -> Result<Self> {
        let syn::DeriveInput {
            attrs,
            ident,
            generics,
            data,
            ..
        } = Parse::parse(input)?;
        let destination_types = get_destinations(&attrs)?;
        let derives = get_additional_derives(&attrs)?;
        if destination_types.is_empty() {
            return Err(Error::new(
                ident.span(),
                r#"No destination types, specify at least one with `#[destinations("TypeName")]`"#,
            ));
        }
        let type_info = TypeInfo::new(data, ident, generics)?;
        Ok(InputData {
            type_info,
            destination_types,
            derives,
        })
    }
}

fn get_destinations(attrs: &[syn::Attribute]) -> Result<Vec<syn::Path>> {
    let mut result = Vec::new();
    for attr in attrs
        .iter()
        .filter(|attr| attr.path.is_ident("destinations"))
    {
        let meta = attr.parse_meta()?;
        let meta_list = match meta {
            syn::Meta::List(list) => list,
            x => return Err(Error::new(x.name().span(), "Expected a list of type names")),
        };
        for nested in meta_list.nested {
            use syn::NestedMeta::*;
            let path = match nested {
                Literal(syn::Lit::Str(lit_str)) => lit_str.parse::<syn::Path>(),
                x => Err(Error::new(
                    x.span(),
                    "Destinations should be a list of quoted types",
                )),
            }?;
            result.push(path);
        }
    }
    Ok(result)
}

fn get_additional_derives(attrs: &[syn::Attribute]) -> Result<Vec<syn::Ident>> {
    let mut result = Vec::new();
    for attr in attrs
        .iter()
        .filter(|attr| attr.path.is_ident("add_derives"))
    {
        let meta = attr.parse_meta()?;
        let meta_list = match meta {
            syn::Meta::List(list) => list,
            meta => return Err(Error::new(meta.span(), "Expected a list of names")),
        };
        for nested in meta_list.nested {
            let id = match nested {
                syn::NestedMeta::Meta(syn::Meta::Word(id)) => id,
                meta => {
                    return Err(Error::new(
                        meta.span(),
                        "Destinations should be a list of quoted types",
                    ));
                }
            };
            result.push(id);
        }
    }
    Ok(result)
}
