use proc_macro2::{Span, TokenStream};
use quote::ToTokens;
use std::fmt;
use syn::{self, spanned::Spanned};

pub enum Fields {
    Named(Vec<Field>),
    Unnamed(Vec<Field>),
    Empty,
}

/// Converts a list of fields from *syn* into a vector of `Field`s.
fn convert_fields(fields: impl IntoIterator<Item = syn::Field>) -> Vec<Field> {
    fields
        .into_iter()
        .enumerate()
        .map(Field::from_syn)
        .collect()
}

impl From<syn::Fields> for Fields {
    fn from(origin: syn::Fields) -> Self {
        match origin {
            syn::Fields::Named(fields) => Fields::Named(convert_fields(fields.named)),
            syn::Fields::Unnamed(fields) => Fields::Unnamed(convert_fields(fields.unnamed)),
            syn::Fields::Unit => Fields::Empty,
        }
    }
}

impl Fields {
    pub fn is_empty(&self) -> bool {
        if let Fields::Empty = self {
            true
        } else {
            false
        }
    }

    pub fn iter(&self) -> impl Iterator<Item = &Field> + Clone {
        match self {
            Fields::Named(f) | Fields::Unnamed(f) => f.iter(),
            Fields::Empty => [].iter(),
        }
    }
}

pub struct Field {
    pub id: FieldId,
    pub ty: syn::Type,
}

#[derive(Clone)]
pub enum FieldId {
    Named(syn::Ident),
    Unnamed(syn::Index),
}

impl Field {
    fn from_syn((index, orig): (usize, syn::Field)) -> Self {
        let span = orig.span();
        let id = orig.ident.map(FieldId::Named).unwrap_or_else(|| {
            FieldId::Unnamed(syn::Index {
                index: index as u32,
                span,
            })
        });
        Field {
            id,
            ty: orig.ty.clone(),
        }
    }

    pub fn get_literal_id(&self) -> syn::Ident {
        match &self.id {
            FieldId::Named(id) => id.clone(),
            FieldId::Unnamed(syn::Index { index, span }) => {
                syn::Ident::new(&format!("id{}", index), *span)
            }
        }
    }
}

impl fmt::Display for FieldId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            FieldId::Named(id) => write!(f, "{}", id),
            FieldId::Unnamed(idx) => write!(f, "{}", idx.index),
        }
    }
}

impl ToTokens for FieldId {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        match self {
            FieldId::Named(id) => id.to_tokens(tokens),
            FieldId::Unnamed(idx) => idx.to_tokens(tokens),
        }
    }
}

impl FieldId {
    pub fn to_ident(&self) -> syn::Ident {
        match self {
            FieldId::Named(id) => id.clone(),
            FieldId::Unnamed(syn::Index { index, span }) => {
                syn::Ident::new(&format!("id{}", index), *span)
            }
        }
    }

    pub fn span(&self) -> Span {
        match self {
            FieldId::Unnamed(syn::Index { span, .. }) => *span,
            FieldId::Named(id) => id.span(),
        }
    }
}
