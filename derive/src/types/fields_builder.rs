use super::field::{Field, Fields};
use proc_macro2::TokenStream;
use quote::{quote, ToTokens};

pub struct FieldsBuilder<'a, F> {
    fields: &'a F,
    tokens: TokenStream,
    semicolon: bool,
}

pub trait FieldMapper {
    type Output: ToTokens;

    fn map_field(&mut self, field: &Field) -> Self::Output;
}

impl<T, O> FieldMapper for T
where
    T: FnMut(&Field) -> O,
    O: ToTokens,
{
    type Output = O;

    fn map_field(&mut self, field: &Field) -> O {
        (self)(field)
    }
}

impl<'a> FieldsBuilder<'a, Fields> {
    pub fn new(fields: &'a Fields) -> Self {
        let semicolon = match fields {
            Fields::Empty | Fields::Unnamed(..) => true,
            Fields::Named(..) => false,
        };
        FieldsBuilder {
            fields,
            tokens: TokenStream::new(),
            semicolon,
        }
    }

    pub fn deconstruct(self, mut fields_mapper: impl FieldMapper) -> FieldsBuilder<'static, ()> {
        let fields = self.fields;
        let mut tokens = self.tokens;
        match fields {
            Fields::Empty => {}
            Fields::Named(fields) => {
                let fields = fields.iter().map(|field| {
                    let id = &field.id;
                    let mapped = fields_mapper.map_field(field);
                    quote!( #id: #mapped )
                });
                quote!( {#( #fields ),*} ).to_tokens(&mut tokens);
            }
            Fields::Unnamed(fields) => {
                let fields = fields.iter().map(|field| {
                    let mapped = fields_mapper.map_field(field);
                    quote!( #mapped )
                });
                quote!( (#( #fields ),*) ).to_tokens(&mut tokens);
            }
        }
        FieldsBuilder {
            fields: &(),
            tokens,
            semicolon: self.semicolon,
        }
    }

    pub fn deconstruct_refs(self) -> TokenStream {
        self.deconstruct(|Field { id, .. }: &Field| {
            let named = id.to_ident();
            quote!(ref #named)
        })
        .tokens()
    }

    pub fn convert<O: ToTokens>(self, conversion: impl Fn(syn::Ident) -> O) -> TokenStream {
        self.deconstruct(|Field { id, .. }: &Field| {
            let literal = id.to_ident();
            conversion(literal)
        })
        .tokens()
    }

    /// Makes a declaration for a data type with the same fields.
    pub fn simple_declaration(self) -> FieldsBuilder<'static, ()> {
        self.deconstruct(|Field { ty, .. }: &Field| quote!(#ty))
    }
}

impl<'a> FieldsBuilder<'a, ()> {
    pub fn tokens(self) -> TokenStream {
        self.tokens
    }
}

impl<'a, F> FieldsBuilder<'a, F> {
    /// Appends a semicolon if required.
    pub fn end(mut self) -> TokenStream {
        if self.semicolon {
            quote!(;).to_tokens(&mut self.tokens)
        }
        self.tokens
    }
}

// impl ToTokens for FieldsBuilder<()> {
//     fn to_tokens(&self, tokens: &mut TokenStream) {
//         tokens.extend(self.tokens.clone());
//     }
//
//     fn into_token_stream(self) -> TokenStream {
//         self.tokens
//     }
// }
