use super::field::Fields;
use syn;

pub struct EnumVariant {
    pub fields: Fields,
    pub ident: syn::Ident,
}

pub struct EnumData {
    pub variants: Vec<EnumVariant>,
}

impl From<syn::Variant> for EnumVariant {
    fn from(syn::Variant { ident, fields, .. }: syn::Variant) -> Self {
        EnumVariant {
            ident,
            fields: Fields::from(fields),
        }
    }
}

impl From<syn::DataEnum> for EnumData {
    fn from(syn::DataEnum { variants, .. }: syn::DataEnum) -> Self {
        EnumData {
            variants: variants.into_iter().map(EnumVariant::from).collect(),
        }
    }
}
