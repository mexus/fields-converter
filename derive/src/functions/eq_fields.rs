use crate::types::{
    enum_data::{EnumData, EnumVariant},
    field::Field,
    fields_builder::FieldsBuilder,
    struct_data::StructData,
    type_data::{DataType, TypeInfo},
};
use proc_macro2::TokenStream;
use quote::{quote, ToTokens};
use syn;

pub fn eq_fields(info: &TypeInfo, ty_dest: &syn::Path) -> TokenStream {
    match &info.ty {
        DataType::Struct(s) => eq_fields_struct(s, &info.name, ty_dest, &info.generics),
        DataType::Enum(e) => eq_fields_enum(e, &info.name, ty_dest, &info.generics),
    }
}

fn eq_fields_struct(
    StructData { fields }: &StructData,
    ty_orig: &syn::Ident,
    ty_dest: &syn::Path,
    generics: &syn::Generics,
) -> TokenStream {
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();
    let cmps = fields
        .iter()
        .map(|Field { id, .. }| quote!( PartialEq::eq(&self.#id, &other.#id) ))
        .fold(quote!(true), |acc, x| quote!( #acc && #x ));
    quote!(
        impl #impl_generics PartialEq<#ty_dest #ty_generics> for #ty_orig #ty_generics #where_clause {
            fn eq(&self, other: &#ty_dest #ty_generics) -> bool {
                #cmps
            }
        }

        impl #impl_generics PartialEq<#ty_orig #ty_generics> for #ty_dest #ty_generics #where_clause {
            fn eq(&self, other: &#ty_orig #ty_generics) -> bool {
                #cmps
            }
        }
    )
}

fn mapping<'a>(prefix: &'a str) -> impl Fn(&Field) -> syn::Ident + 'a + Copy {
    move |Field { id, .. }: &Field| syn::Ident::new(&format!("{}{}", prefix, id), id.span())
}

fn compare_variants(
    variants: &[EnumVariant],
    ty_left: impl ToTokens,
    ty_right: impl ToTokens,
) -> TokenStream {
    let map_left = mapping("left_");
    let map_right = mapping("right_");
    variants
        .iter()
        .map(move |EnumVariant { fields, ident }| {
            let matching_left = FieldsBuilder::new(fields).deconstruct(map_left).tokens();
            let matching_right = FieldsBuilder::new(fields).deconstruct(map_right).tokens();
            let cmps = fields
                .iter()
                .map(|field| {
                    let left = map_left(field);
                    let right = map_right(field);
                    quote!( #left == #right )
                })
                .fold(quote!(true), |acc, x| quote!( #acc && #x ));
            quote!( (#ty_left::#ident #matching_left, #ty_right::#ident #matching_right) => #cmps, )
        })
        .fold(TokenStream::new(), |acc, x| quote!( #acc #x ))
}

fn eq_fields_enum(
    EnumData { variants }: &EnumData,
    ty_orig: &syn::Ident,
    ty_dest: &syn::Path,
    generics: &syn::Generics,
) -> TokenStream {
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();
    let cmps = compare_variants(variants, ty_orig, ty_dest);
    let cmps_reversed = compare_variants(variants, ty_dest, ty_orig);
    quote!(
        impl #impl_generics PartialEq<#ty_dest #ty_generics> for #ty_orig #ty_generics
        #where_clause {
            fn eq(&self, other: &#ty_dest #ty_generics) -> bool {
                match (self, other) {
                    #cmps
                    _ => false,
                }
            }
        }

        impl #impl_generics PartialEq<#ty_orig #ty_generics> for #ty_dest #ty_generics
        #where_clause {
            fn eq(&self, other: &#ty_orig #ty_generics) -> bool {
                match (self, other) {
                    #cmps_reversed
                    _ => false,
                }
            }
        }
    )
}
