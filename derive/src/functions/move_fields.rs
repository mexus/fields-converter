use crate::types::{
    enum_data::{EnumData, EnumVariant},
    field::Field,
    fields_builder::FieldsBuilder,
    struct_data::StructData,
    type_data::{DataType, TypeInfo},
};
use proc_macro2::TokenStream;
use quote::{quote, ToTokens};
use syn;

pub fn move_fields(info: &TypeInfo, to_ty: &syn::Path) -> impl ToTokens {
    match &info.ty {
        DataType::Struct(s) => move_fields_struct(s, &info.name, &info.generics, to_ty),
        DataType::Enum(e) => move_enum(e, &info.name, &info.generics, to_ty),
    }
}

fn move_fields_struct(
    StructData { fields }: &StructData,
    from_ty: &syn::Ident,
    generics: &syn::Generics,
    to_ty: &syn::Path,
) -> TokenStream {
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();
    let deconstructed = FieldsBuilder::new(fields)
        .deconstruct(Field::get_literal_id)
        .tokens();
    let into = FieldsBuilder::new(fields).convert(|origin| quote!(Into::into(#origin)));
    let from = FieldsBuilder::new(fields).convert(|origin| quote!(From::from(#origin)));
    quote!(
        impl #impl_generics Into<#to_ty #ty_generics> for #from_ty #ty_generics #where_clause {
            fn into(self) -> #to_ty #ty_generics {
                let #from_ty #deconstructed = self;
                #to_ty #into
            }
        }

        impl #impl_generics From<#to_ty #ty_generics> for #from_ty #ty_generics #where_clause {
            fn from(#to_ty #deconstructed: #to_ty #ty_generics) -> Self {
                #from_ty #from
            }
        }
    )
}

fn move_enum(
    EnumData { variants }: &EnumData,
    from_ty: &syn::Ident,
    generics: &syn::Generics,
    to_ty: &syn::Path,
) -> TokenStream {
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();
    let variants_into = variants.iter().map(|EnumVariant { fields, ident }| {
        let matching = FieldsBuilder::new(fields)
            .deconstruct(Field::get_literal_id)
            .tokens();
        let converted = FieldsBuilder::new(fields).convert(|origin| quote!(Into::into(#origin)));
        quote!( #from_ty::#ident #matching => #to_ty::#ident #converted )
    });
    let variants_from = variants.iter().map(|EnumVariant { fields, ident }| {
        let matching = FieldsBuilder::new(fields)
            .deconstruct(Field::get_literal_id)
            .tokens();
        let converted = FieldsBuilder::new(fields).convert(|origin| quote!(From::from(#origin)));
        quote!( #to_ty::#ident #matching => #from_ty::#ident #converted )
    });
    quote!(
        impl #impl_generics Into<#to_ty #ty_generics> for #from_ty #ty_generics #where_clause {
            fn into(self) -> #to_ty #ty_generics {
                match self {
                    #( #variants_into ),*
                }
            }
        }

        impl #impl_generics From<#to_ty #ty_generics> for #from_ty #ty_generics #where_clause {
            fn from(other: #to_ty #ty_generics) -> Self {
                match other {
                    #( #variants_from ),*
                }
            }
        }
    )
}
