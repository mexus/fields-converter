use crate::types::{
    enum_data::{EnumData, EnumVariant},
    field::{Field, Fields},
    fields_builder::FieldsBuilder,
    struct_data::StructData,
    type_data::{DataType, TypeInfo},
};
use itertools::Itertools;
use proc_macro2::TokenStream;
use quote::{quote, ToTokens};
use syn;

pub fn ord_fields(info: &TypeInfo, ty_dest: &syn::Path) -> TokenStream {
    match &info.ty {
        DataType::Struct(s) => ord_fields_struct(s, &info.name, ty_dest, &info.generics),
        DataType::Enum(e) => ord_fields_enum(e, &info.name, ty_dest, &info.generics),
    }
}

fn ord_fields_struct(
    StructData { fields }: &StructData,
    ty_orig: &syn::Ident,
    ty_dest: &syn::Path,
    generics: &syn::Generics,
) -> TokenStream {
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();
    let cmps = fields
        .iter()
        .map(|Field { id, .. }| quote!( PartialOrd::partial_cmp(&self.#id, &other.#id) ));
    let cmps_reverse = cmps.clone();
    quote!(
        impl #impl_generics PartialOrd<#ty_dest #ty_generics> for #ty_orig #ty_generics
        #where_clause {
            fn partial_cmp(&self, other: &#ty_dest #ty_generics) -> Option<::std::cmp::Ordering> {
                Some(::std::cmp::Ordering::Equal)
                    #(.and_then(|prev| #cmps.map(|next| prev.then(next))))*
            }
        }

        impl #impl_generics PartialOrd<#ty_orig #ty_generics> for #ty_dest #ty_generics
        #where_clause {
            fn partial_cmp(&self, other: &#ty_orig #ty_generics) -> Option<::std::cmp::Ordering> {
                Some(::std::cmp::Ordering::Equal)
                    #(.and_then(|prev| #cmps_reverse.map(|next| prev.then(next))))*
            }
        }
    )
}

fn mapping<'a>(prefix: &'a str) -> impl Fn(&Field) -> syn::Ident + 'a + Copy {
    move |Field { id, .. }: &Field| syn::Ident::new(&format!("{}{}", prefix, id), id.span())
}

fn compare_fields(
    fields: &Fields,
    left_ident: &syn::Ident,
    right_ident: &syn::Ident,
    ty_orig: impl ToTokens,
    ty_dest: impl ToTokens,
) -> TokenStream {
    let map_left = mapping("left_");
    let map_right = mapping("right_");
    let matching_left = FieldsBuilder::new(fields).deconstruct(map_left).tokens();
    let matching_right = FieldsBuilder::new(fields).deconstruct(map_right).tokens();
    let cmps = fields
        .iter()
        .map(|field| {
            let left = map_left(field);
            let right = map_right(field);
            quote!( PartialOrd::partial_cmp(&#left, &#right) )
        })
        .fold(
            quote!(Some(::std::cmp::Ordering::Equal)),
            |acc, x| quote!( #acc.and_then(|prev| #x.map(|next| prev.then(next)))),
        );
    quote!((
             #ty_orig::#left_ident #matching_left,
             #ty_dest::#right_ident #matching_right
            ) =>
            #cmps, )
}

fn compare_variants(
    variants: &[EnumVariant],
    ty_orig: &impl ToTokens,
    ty_dest: &impl ToTokens,
) -> TokenStream {
    let iter = variants.iter().enumerate();
    iter.clone()
        .cartesian_product(iter)
        .map(|((left_n, left), (right_n, right))| {
            let left_ident = &left.ident;
            let right_ident = &right.ident;
            if left_n == right_n {
                compare_fields(&left.fields, left_ident, right_ident, ty_orig, ty_dest)
            } else if left_n < right_n {
                quote!((#ty_orig::#left_ident {..}, #ty_dest::#right_ident {..}) =>
                       Some(::std::cmp::Ordering::Less), )
            } else {
                quote!((#ty_orig::#left_ident {..}, #ty_dest::#right_ident {..}) =>
                       Some(::std::cmp::Ordering::Greater), )
            }
        })
        .fold(TokenStream::new(), |acc, x| quote!(#acc #x))
}

fn ord_fields_enum(
    EnumData { variants }: &EnumData,
    ty_orig: &syn::Ident,
    ty_dest: &syn::Path,
    generics: &syn::Generics,
) -> TokenStream {
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();
    let cmp = compare_variants(variants, ty_orig, ty_dest);
    let cmp_reverse = compare_variants(variants, ty_dest, ty_orig);
    quote!(
        impl #impl_generics PartialOrd<#ty_dest #ty_generics> for #ty_orig #ty_generics
        #where_clause {
            fn partial_cmp(&self, other: &#ty_dest #ty_generics) -> Option<::std::cmp::Ordering> {
                match (self, other) {
                    #cmp
                }
            }
        }

        impl #impl_generics PartialOrd<#ty_orig #ty_generics> for #ty_dest #ty_generics
        #where_clause {
            fn partial_cmp(&self, other: &#ty_orig #ty_generics) -> Option<::std::cmp::Ordering> {
                match (self, other) {
                    #cmp_reverse
                }
            }
        }
    )
}
