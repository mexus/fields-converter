use crate::types::{
    enum_data::{EnumData, EnumVariant},
    fields_builder::FieldsBuilder,
    struct_data::StructData,
    type_data::{DataType, TypeInfo},
};
use proc_macro2::TokenStream;
use quote::{quote, ToTokens};
use syn;

pub fn construct_type(info: &TypeInfo, name: &syn::Path, derives: &[syn::Ident]) -> impl ToTokens {
    match &info.ty {
        DataType::Struct(s) => construct_struct(s, name, &info.generics, derives),
        DataType::Enum(e) => construct_enum(e, name, &info.generics, derives),
    }
}

fn construct_struct(
    StructData { fields }: &StructData,
    name: &syn::Path,
    generics: &syn::Generics,
    derives: &[syn::Ident],
) -> TokenStream {
    let (_impl_generics, ty_generics, where_clause) = generics.split_for_impl();
    let fields = FieldsBuilder::new(fields).simple_declaration().end();
    quote!(
        #[derive(#(#derives),*)]
        struct #name #ty_generics #where_clause #fields
    )
}

fn construct_enum(
    EnumData { variants }: &EnumData,
    name: &syn::Path,
    generics: &syn::Generics,
    derives: &[syn::Ident],
) -> TokenStream {
    let (_impl_generics, ty_generics, where_clause) = generics.split_for_impl();
    let variants = variants.iter().map(|EnumVariant { fields, ident }| {
        let fields = FieldsBuilder::new(fields).simple_declaration().tokens();
        quote!( #ident #fields )
    });
    quote!(
        #[derive(#(#derives),*)]
        enum #name #ty_generics #where_clause {
            #( #variants ),*
        }
    )
}
