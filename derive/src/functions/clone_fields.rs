use crate::types::{
    enum_data::{EnumData, EnumVariant},
    fields_builder::FieldsBuilder,
    struct_data::StructData,
    type_data::{DataType, TypeInfo},
};
use proc_macro2::TokenStream;
use quote::{quote, ToTokens};
use syn;

pub fn clone_fields(info: &TypeInfo, to_ty: &syn::Path) -> impl ToTokens {
    match &info.ty {
        DataType::Struct(s) => clone_fields_struct(s, &info.name, &info.generics, to_ty),
        DataType::Enum(e) => clone_fields_enum(e, &info.name, &info.generics, to_ty),
    }
}

fn clone_fields_struct(
    StructData { fields }: &StructData,
    from_ty: &syn::Ident,
    generics: &syn::Generics,
    to_ty: &syn::Path,
) -> TokenStream {
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();
    let deconstructed = FieldsBuilder::new(fields).deconstruct_refs();
    let direct_clones =
        FieldsBuilder::new(fields).convert(|origin| quote!( CloneInto::clone_into(#origin) ));
    let reverse_clones =
        FieldsBuilder::new(fields).convert(|origin| quote!( CloneFrom::clone_from(#origin) ));
    quote!(
        impl #impl_generics CloneInto<#to_ty #ty_generics> for #from_ty #ty_generics #where_clause {
            fn clone_into(&self) -> #to_ty #ty_generics {
                let &#from_ty #deconstructed = self;
                #to_ty #direct_clones
            }
        }

        impl #impl_generics CloneFrom<#to_ty #ty_generics> for #from_ty #ty_generics #where_clause {
            fn clone_from(&#to_ty #deconstructed: &#to_ty #ty_generics) -> Self {
                #from_ty #reverse_clones
            }
        }
    )
}

fn clone_fields_enum(
    EnumData { variants }: &EnumData,
    from_ty: &syn::Ident,
    generics: &syn::Generics,
    to_ty: &syn::Path,
) -> TokenStream {
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();
    let variants_into = variants.iter().map(|EnumVariant { fields, ident }| {
        let matching = FieldsBuilder::new(fields).deconstruct_refs();
        let converted =
            FieldsBuilder::new(fields).convert(|origin| quote!(CloneInto::clone_into(#origin)));
        quote!( #from_ty::#ident #matching => #to_ty::#ident #converted )
    });
    let variants_from = variants.iter().map(|EnumVariant { fields, ident }| {
        let matching = FieldsBuilder::new(fields).deconstruct_refs();
        let converted =
            FieldsBuilder::new(fields).convert(|origin| quote!(CloneFrom::clone_from(#origin)));
        quote!( #to_ty::#ident #matching => #from_ty::#ident #converted )
    });
    quote!(
        impl #impl_generics CloneInto<#to_ty #ty_generics> for #from_ty #ty_generics #where_clause {
            fn clone_into(&self) -> #to_ty #ty_generics {
                match self {
                    #( #variants_into ),*
                }
            }
        }

        impl #impl_generics CloneFrom<#to_ty #ty_generics> for #from_ty #ty_generics #where_clause {
            fn clone_from(other: &#to_ty #ty_generics) -> Self {
                match other {
                    #( #variants_from ),*
                }
            }
        }
    )
}
